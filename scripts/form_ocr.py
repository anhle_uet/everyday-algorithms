import argparse
import cv2
import os

import .preprocess
import .gen_dict.py
import .parse_text

keys = ['Công ty',
        'Dự án',
        'Tầng',
        'Người phụ trách',
        'Điện thoại',
        'Công việc yêu cầu',
        'Người đăng ký',
        'Ngày']

alts = ['Company',
        'Project',
        'Level',
        'Person in charge',
        'Telephone',
        'Nature of request',
        'Request by',
        'Date']

class Configure:
  def __init__(self, threshold=0.7):
    sef.threshold = threshold

def form_image_to_string(img, config=None):
  text = ''
  return text

def get_roi_candidates():
  pass

def get_company_roi():
  pass

def get _project_roi():
  pass

def get_level_roi():
  pass

def get_person_in_charge_roi():
  pass

def get_telephone_roi():
  pass

def get_nature_of_request_roi():
  pass

def get_request_by_roi():
  pass

def get_date_roi():
  pass

def get_company_text():
  pass

def get _project_text():
  pass

def get_level_text():
  pass

def get_person_in_charge_text():
  pass

def get_telephone_text():
  pass

def get_nature_of_request_text():
  pass

def get_request_by_text():
  pass

def get_date_text():
  pass