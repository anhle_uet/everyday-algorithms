import argparse
import cv2
import os
import json

import .form_ocr

if __name__ == '__main__':
  parser = ArgumentParser()
  parser.add_argument('-s', '--src', help='path to the directory of form images')
  parser.add_argumetn('-d', '--dst', help='path to the directory to save json files')

  args = parser.parse_args()
  src_dir = os.path.abspath(args.src)
  dst_dir = os.path.abspath(args.dst)

  for file in os.listdir(src_dir):
    name, ext = os.path.splitext(file)
    img = cv2.imread(os.path.join(file, src_dir))
    result = form_ocr.form_image_to_string(img)

    json_file = os.path.join(dst_dir, name + '.json')
    with open(json_file, 'w+', encoding='utf-8') as f:
      json.dumps(result, file=f, separators=(',', ':'))